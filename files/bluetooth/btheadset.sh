#!/bin/bash

# Generar información de un dispositivo bluetooth headphones.
infobt() {

	info=$(echo -e "info $1\n" | bluetoothctl)
	sleep 1
}

# Encender bluetoothctl.
poweronbt() {

	infobt $1

	# Si bluetoothctl no está encendido. 
	if [[ `echo -e "show\n" | bluetoothctl` =~ 'Powered: no' ]]; then

		echo -e "power on\n" | bluetoothctl
		sleep 3

		infobt $1

		# Si bluetoothctl está encendido. 
		if [[ `echo -e "show\n" | bluetoothctl` =~ 'Powered: yes' ]]; then

			echo "Bluetoothctl encendido correctamente, emparejando..."
			pair $1
		else
			echo "Bluetoothctl no se ha podido encender."
		fi
	else
		echo "Bluetoothctl previamente encendido, emparejando..."
		pair $1
	fi

}
# Emparejara dispositivo bluetooth headphones.
pair() {

	if [[ `echo $info | grep "Paired: no"` ]]; then

		echo -e "pair $1\n" | bluetoothctl
		sleep 3

		infobt $1

		# Si ha emparejado correctamente.
		if [[ `echo $info | grep "Paired: yes"` ]]; then

			#echo "Dispositivo emparejado correctaemente"
			connectbt $1
		else
			echo "No ha sido posible emparejar el dispositivo '$1', verifique que esté correctamente encendido."
		fi
	else
		echo "Dispositivo '$1' previamente emparejado, conectando..."
		connectbt $1
	fi
}


# Asignar como confiable dispositivo bluetooth headphones.
trust() {

	infobt $1

	# Si no ha sido marcado como confiable.
	if [[ `echo $info | grep -ow "Trusted: no"` ]]; then

		echo -e "trust $1\n" | bluetoothctl
		sleep 3
	else
		echo "Dispositivo '$1' ha sido previamente marcado como confiable."
	fi
}


#Gestionar bluetoothctl estando ya encendido.
bluetoothctl(){

	paired_devices=$(echo -e "paired-devices\n" | bluetoothctl)

	# Dispositivos bluetooth MAC.
	anker_soundcore_q30='AC:12:2F:0B:3F:76'

	if [[ `echo $paired_devices | grep -ow "$anker_soundcore_q30"` ]]; then

		#echo "ANKER SoundCore emparejados, debe conectar."
		connectbt $anker_soundcore_q30
	else
		#echo "ANKER SoundCore no emparejados, debe emparejar y connectar '$anker_soundcore_q30'."

		pair $anker_soundcore_q30
		infobt $anker_soundcore_q30

		# Si el dispositivo ya esta emparejado.
		if [[ `echo $info | grep -ow "Paired: yes"` ]]; then

			connectbt $anker_soundcore_q30
		else
			echo "No ha sido posible emparejar el dispositivo '$anker_soundcore_q30', verifique que esté correctamente encendido."	
		fi
	fi
}


# Conectar dispositivio bluetooth headphones.
connectbt() {

	infobt $1

	# Si el dispositivo no está conectado.
	if [[ `echo $info | grep -ow "Connected: no"` ]]; then

		echo -e "connect $1\n" | bluetoothctl
		sleep 3

		if [[ `echo $info | grep -ow "Connected: yes"` ]]; then

			echo "Dispositivo '$1' conectado."
			trust $1
		else
			echo "No se ha podido conectar el dispositivo $1, verifique que esté correctamente encendido."
		fi

	else
		echo "Dispositivo '$1' conectado."
		trust $1
	fi

	# Si no ha sido marcado como confiable.
}


# Si el servicio de bluetooth está inactivo.
if [[ `systemctl is-active bluetooth.service` -eq 'inactive' ]]; then

	echo "Bluetooth inactivo, activando..."
	sudo systemctl start bluetooth.service
	pulseaudio --start
	sleep 3
	echo "bluetooth activándose"

	anker_soundcore_q30='AC:12:2F:0B:3F:76'
	#poweronbt $anker_soundcore_q30
	#echo "Power on ejecutado"

	var=$(echo -e "show\n" | bluetoothctl)

	echo "var=$var"
	#if [[ `echo "$(echo -e "show\n" | bluetoothctl)" | grep -ow 'Powered: yes'` ]]; then
	if [[ `echo "$var" | grep -ow 'Powered: yes'` ]]; then

	# Configuración bluetoothctl.
	#if [[ `echo  "$show"` =~ 'Powered: yes' ]]; then 
	#if [[ `echo  "$show" | grep -ow 'Powered: yes'` ]]; then 
	#if [[ `echo -e "show\n" | bluetoothctl | grep -ow 'Powered: yes'` ]]; then 
		
		echo "Bluetooth activo"
		anker_soundcore_q30='AC:12:2F:0B:3F:76'
		#poweronbt $anker_soundcore_q30
	else
		echo "No se ha podido encender bluetoothctl correctamente, verifique el estado del servicio 'bluetooth.service'."
	fi
	!
fi

:<<-!
!










#sudo systemctl stop bluetooth.service && pulseaudio -k

