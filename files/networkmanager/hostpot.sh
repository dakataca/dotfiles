#!/bin/bash

# Script para crear wifi hostpot a partir de la red cableada.

output="$(nmcli d s)"

IFNAME="wlan0"
SSID="MATARIFE"
PASSWD="p4r4qu1+0m4+4r1f3"


# Crear perfil de red.
create_wifi_profile() {

	nmcli c add type wifi ifname $IFNAME con-name $SSID autoconnect no ssid $SSID 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared wifi-sec.key-mgmt wpa-psk wifi-sec.psk $PASSWD 

}


hostpot(){

	if [[ `nmcli con show | grep -ow "$SSID"` ]]; then
		
		echo "Perfil de red \"$SSID\" ya existe."
		nmcli con up MATARIFE 
	else
		echo "Perfil de red \"$SSID\" aun no existe."
		create_wifi_profile
		sleep 1
		nmcli con up $SSID 
	fi

}


switch_hostpot(){

	# Si el wifi está conectado.
	if [[ `echo "$output" | awk '/^wl/{print $3}'` = "conectado" ]]; then 
 
		# Deshabilitar conexión activa con nmcli.
		#nmcli con down "$(nmcli d s | awk '/^wl/{for (i=1; i<=NF-3; i++) $i = $(i+3); NF-=3; print}')"
		hostpot
	else
		echo "No hacer nada."
		nmcli d s
		hostpot
	fi

}


# Si el cable de red está conectado.
if [[ `echo "$output" | awk '/^en/{print $3}'` = "conectado" ]]; then 

	echo "Cable de red conectado."

	if [[ `echo "$output" | awk '/^wl/{for (i=1; i<=NF-2; i++) $i = $(i+2); NF-=3; print}'` = "no disponible" ]]; then 

		#Enciendo el dispositivo inalámbrico con la utilidad nmcli.
		nmcli radio wifi on && sleep 4
		switch_hostpot
	else
		switch_hostpot
	fi
else
	echo "Cable de red desconectado."
fi
