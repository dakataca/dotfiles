#!/bin/sh

LAN_INTERFACE=$(ls /sys/class/net/ | grep '^en.\+')

if [ "$1" = "$LAN_INTERFACE" ]; then
	case "$2" in
        up)
		nmcli radio wifi off
        ;;
        down)
		nmcli radio wifi on
		#sudo systemctl restart NetworkManager.service
	;;
	esac
fi
