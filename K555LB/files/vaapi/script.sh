!#/bin/bash

echo "LIBVA_DRIVER_NAME=iHD" >> /etc/environment

pacman -S intel-media-driver libva-utils intel-gpu-tools vdpauinfo libvdpau-va-gl mpv youtube-dl --needed --noconfirm
