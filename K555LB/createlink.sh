!#/bin/bash

# Coexistencia del bluetooth y el wifi
ln -sf bluetooth/iwlwifi.conf /etc/modprobe.d/iwlwifi.conf

# Wifi speed down asus laptop
ln -sf modules/asus_nb_wmi.conf  /etc/modprobe.d/asus_nb_wmi.conf
