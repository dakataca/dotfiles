#!/bin/bash

#XORG=(xorg-{xdpyinfo,xwininfo,xinput,xkill,xdriinfo,xlsclients,xprop,xset,server,xinit,xlsfonts,xvinfo,mkfontscale,fonts-type1} otf-latin-modern xterm)
#WM=(qtile python2)

INTEL_DRIVER=(intel-gpu-tools vulkan-intel)
VAAPI=(intel-media-driver libva-utils vdpauinfo libvdpau-va-gl)

#SOUND_BLUETOOTH=(alsa-utils pulseaudio-{alsa,bluetooth} bluez-utils)
#MULTIMEDIA=(cmus mpv atomicparsley mplayer mediainfo youtube-dl scrot ristretto)
MULTIMEDIA=(mpv)
#MESSAGING=(telegram-desktop)
#INTERNET=(chromium qbittorrent)
#ANDROID=(android-tools)
#SYSTEM_TOOLS=(htop thunar thunar-volman acp{i,id} net-tools usbutils)
#USER_TOOLS=(neofetch bc nmap speedtest-cli)
#sudo pacman -Syu  ${XORG[@]} ${WM[@]} ${INTEL_DRIVER[@]} ${VAAPI[@]} ${SOUND_BLUETOOTH[@]} ${MULTIMEDIA[@]} ${MESSAGING[@]} ${INTERNET[@]} ${ANDROID[@]} ${SYSTEM_TOOLS[@]} ${USER_TOOLS[@]} --needed --noconfirm

sudo pacman -Syu  ${INTEL_DRIVER[@]} ${VAAPI[@]} ${MULTIMEDIA[@]} --needed --noconfirm
