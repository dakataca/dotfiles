#!/bin/bash

# Este script insertará líneas en ficheros de configuración.
# Obtendrá existencia única de una línea en un fichero.
# Actualizar líneas de configuración.

declare -a list=("$@")	

# Config file.
FILE="${list[$(($#-1))]}"

# Validation of configuration lines.
validate_lines(){

	#for line in "$@"
	for line in "${list[@]}"
	do

		# I replace special characters.
		sline=$(echo "$line" | sed 's/\[/\\\[/g ; s/\]/\\\]/g ; s/\//\\\//g ; s/--\+/\\&/g')

		# Si la línea de configuración no inicia y contiene el simbolo =, seguido de cualquier caracter o ninguno.
		if [[ `echo "$sline" | grep '.\+=.*'` ]]; then

			# Extraigo desde el inicio de la linea hasta el igual.
			xline=$(echo "$sline" | grep -o '.\+=')
		else
			# Establesco la línea con caracteres sustitudidos en la variable que validará existencias.
			xline=$sline
		fi

		# If the line exists in the file. 
		if [[ `grep -o "^$xline" $FILE` ]]; then

			# Mientras exista más de una línea en el fichero.	
			while [ $(grep -c "^$xline" $FILE) -gt 1  ]
			do

				# Obtengo número de la última línea repetida.
				repeatLine=$(sed -n "/$xline/=" $FILE | tail -1)

				# Si existe linea repetida.
				if [[ $repeatLine != "" ]]; then

					if [[ ! `echo "$FILE" | egrep -o "^/home"` ]]; then

						# Elimino la línea repetida con permisos root.	
						sudo sed -i ${repeatLine}d $FILE
					else
						sed -i ${repeatLine}d $FILE
					fi

 					echo -e "\e[34;1m ->\e[37m Se eliminó la línea repetida $repeatLine.\e[0m"
				fi

			done

			# Obtengo la línea del fichero de configuración.
			lineFile="$(grep "^$xline" $FILE)"

			# Si la línea de los dotfiles es distinta de la establecida en el fichero de configuración.
			if [[ "$line" != "$lineFile" ]]; then

				# Si no es una ruta del espacio del usuario.
				if [[ ! `echo "$FILE" | egrep -o "^/home"` ]]; then

					# Cambio línea desactualizada con permisos root.
					sudo sed -i /$lineFile/c\\$line $FILE
 					echo -e "\e[34;1m  -->\e[37m Línea '\e[0m$lineFile\e[37;1m' actualizada por '\e[0m$line\e[37;1m' en '\e[32m$FILE\e[37m'.\e[0m"
				else
					# Cambio línea desactualizada en el espacio del usuario.
					sed -i /$lineFile/c\\$line $FILE
 					echo -e "\e[34;1m  -->\e[37m Línea '\e[0m$lineFile\e[37;1m' actualizada por '\e[0m$line\e[37;1m' en '\e[32m$FILE\e[37m'.\e[0m"
				fi


			fi

		else
			# if it's not a directory.
			if [ ! -f "$line" ]; then

				# Si no es una ruta del espacio del usuario.
				if [[ ! `echo "$FILE" | egrep -o "^/home"` ]]; then

					# Inserto línea en ruta con permisos root.
					echo "$line" | sudo tee -a $FILE > /dev/null
				else
					# Inserto línea en el espacio del usuario.
					echo "$line" >> $FILE
				fi

 				echo -e "\e[34;1m  -->\e[37m Línea '\e[0m$line\e[37;1m' insertada en '\e[32m$FILE\e[37m'.\e[0m"
			fi
		fi
	done

	#echo -e "\e[32;1m::\e[37m Fichero '\e[32m$FILE\e[37m' establecido con líneas no repetidas.\e[0m"
	echo -e "\e[32;1m ==>\e[37m Fichero '\e[32m$FILE\e[37m' establecido con líneas no repetidas.\e[0m"


}

# If the file exists.
if [ -f $FILE ]; then

	validate_lines

else
	echo -e "\e[33;1mWarning:\e[37m Fichero '\e[32m$FILE\e[37m' no existe, creando fichero... \e[0m"

	# Si no es una ruta del espacio del usuario.
	if [[ ! `echo "$FILE" | egrep -o "^/home"` ]]; then
	
		# Creo fichero con permisos root.
		sudo touch "$FILE"
		echo -e "\e[32;1m::\e[37m Fichero '\e[32m$FILE\e[37m' creado.\e[0m"
	else
		# Creo fichero.
		touch "$FILE"
		echo -e "\e[32;1m::\e[37m Fichero '\e[32m$FILE\e[37m' creado.\e[0m"
	fi

	validate_lines

fi	
