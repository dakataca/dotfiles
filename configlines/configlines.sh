#!/bin/bash

# Paquetes y aplicaciones.
XORG_SERVER=(xorg-{xdpyinfo,xwininfo,xinput,xkill,xdriinfo,xlsclients,xprop,xset,server,xinit,xlsfonts,xvinfo,mkfontscale,fonts-type1} otf-latin-modern xterm)
WMQTILE=(qtile python2)
WMSWAY=(sway{lock,idle,} xorg-xwayland dmenu alacritty grim slurp wl-clipboard wf-recorder gammastep swappy)
DEGNOME=(gnome gnome-extra gdm network-manager-applet)
INTEL_XORG_DRIVER=(xf86-video-intel)
INTEL_UTILS=(intel-gpu-tools)
ARCHIVING=(zip unzip unrar unarj p7zip lzop lzip lrzip lha cpio arj)
VAAPI=(libva-intel-driver libva-utils vdpauinfo libvdpau-va-gl)
SOUND_BLUETOOTH=(alsa-utils pipewire-{alsa,pulse} bluez{-utils,})
MULTIMEDIA=(cmus mpv atomicparsley mplayer mediainfo youtube-dl peek)
SCREEN_CAPTURE_XORG=(scrot ristretto obs-studio simplescreenredorder)
SCREEN_CAPTURE_WAYLAND=(obs-studio v4l2loopback-dkms)
EDITION=(audacity gimp kdenlive breeze)
MESSAGING=(telegram-desktop)
INTERNET=(chromium qbittorrent uget)
FILE_MANAGER_TOOLS=(thunar{-archive-plugin,-volman,} tumbler xarchiver)
ANDROID=(android-tools heimdall)
SYSTEM_TOOLS=(linux linux-headers htop acpi net-tools usbutils tree reflector wget jq dnsmasq light)
USER_TOOLS=(neofetch bc nmap speedtest-cli tmux)


# Functions thanks to Philip Huppert (archvm.sh)
check_fail (){
	if [[  $1 -ne $TRUE  ]]; then

		>&2 echo -e '\e[31;1mFail!\e[0m'
		#exit 1
#	else
#		>&2 echo -e '\e[32;1mDone!\e[0m'
	fi
}


# Deshabilitar GPU Nvidia en Xorg.
disable_gpu_nvidia (){

	# Si la sesión gráfica es Xorg.
	if [ $XDG_SESSION_TYPE = "x11" ]; then

		sudo pacman -S bumblebee nvidia nvidia-utils --needed --noconfirm

		local blacklist=("blacklist nouveau")

		source configlines/insertlines.sh "${blacklist[@]}" /etc/modprobe.d/nobeep.conf

		bbswitch=("options bbswitch load_state=0 unload_state=1")

		source configlines/insertlines.sh "${bbswitch[@]}" /etc/modprobe.d/bbswitch.conf

		echo -e "\e[33;1mWarning:\e[37m Por favor reinicie el equipo para aplicar la deshabilitación de la tarjeta gráfica Nvidia \e[0m"
	fi
}


# Get processors and manufacturer and family.
get_procesor_features (){

	processor_manufacturer=$(awk '/vendor_id/{see[$3]} END{for (l in see) {print l}}'  /proc/cpuinfo)
	processor_family=$(awk '/cpu family/{see[$4]} END{for (l in see) {print l}}'  /proc/cpuinfo)
	processor_type=$(awk  '/model name/{a[$5]} END{for (k in a) {print k}}'  /proc/cpuinfo)

	if [[  $processor_manufacturer == "GenuineIntel" ]]; then
		
		echo "El fabricante del procesador es intel."

		if [[  $processor_family -eq 6 ]]; then
			
			echo "Procesador de $processor_family familia de $processor_manufacturer del tipo $processor_type." 
		fi


	elif [[  $processor_manufacturer == "AuthenticAMD" ]]; then

		echo "El fabricante del procesador es AMD."

	else
		echo "Fabricante  del procesador desconocido".
	fi


}

# Settings for qtile Windows Manager witch xorg.
qtile_config (){

	echo -e "\e[33;1m::\e[37m Sesión gráfica Xorg.\e[0m"

	fix_acpid
	check_fail $?

	disable_gpu_nvidia
	check_fail $?

	#Touchpad.
	sudo cp -u "files/touchpad/30-touchpad.conf" /etc/X11/xorg.conf.d/30-touchpad.conf
	check_fail $?
}


# Settings for sway/wayland Windows Manager.
sway_config (){

	echo -e "\e[33;1m::\e[37m Sesión gráfica wayland.\e[0m"

	# WebRTC on sway chromium: https://wiki.archlinux.org/index.php/PipeWire#WebRTC_screen_sharing
	local webrtc=("XDG_CURRENT_DESKTOP=sway")
	source configlines/insertlines.sh "${webrtc[@]}" /etc/environment

	# touchpad configuration.

	if [[ `grep "input type:touchpad {" ~/.config/sway/config` ]]; then

		echo -e "\e[32;1m::\e[37m Touchpad OK!\e[0m"
	else
		sed -i '/Input configuration/a\input type:touchpad { \
	dwt enabled \
	tap enabled \
	natural_scroll enabledevents \
	events disabled_on_external_mouse \
	scroll_factor 0.3 \
	drag enabled \
	drag_lock enabled \
	middle_emulation enabled \
}' ~/.config/sway/config
		echo -e "\e[32;1m::\e[37m Touchpad OK!\e[0m"

	fi
}


# Apps for qtile in xorg.
qtile_apps (){

	sudo pacman -Syu ${WMQTILE[@]} ${INTEL_XORG_DRIVER[@]} ${INTEL_UTILS[@]} ${ARCHIVING[@]} ${FILE_MANAGER_TOOLS[@]} ${VAAPI[@]} ${SOUND_BLUETOOTH[@]} ${MULTIMEDIA[@]} ${SCREEN_CAPTURE_XORG[@]} ${EDITION[@]} ${MESSAGING[@]} ${INTERNET[@]} ${ANDROID[@]} ${SYSTEM_TOOLS[@]} ${USER_TOOLS[@]} --needed --noconfirm

}


sway_apps (){

	sudo pacman -Syu ${WMSWAY[@]} ${INTEL_UTILS[@]} ${ARCHIVING[@]} ${VAAPI[@]} ${SOUND_BLUETOOTH[@]} ${MULTIMEDIA[@]} ${EDITION[@]} ${MESSAGING[@]} ${INTERNET[@]} ${ANDROID[@]} ${SYSTEM_TOOLS[@]} ${USER_TOOLS[@]} --needed --noconfirm

}


# Establecer prioridad de arranque del kernel linux-lts y linux.
grub_config (){
	
	local count=0
	local left=$1
	local right=$2

	# Cambiar el tiempo de espera del grub.
	if [[ !  `grep -ow "^GRUB_TIMEOUT=3" /etc/default/grub` ]]; then

		# Establecer prioridad del kernel lts
		sudo sed -i 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=3/' /etc/default/grub
		((count+=1))
	fi

	# Establecer prioridad del kernel lts
	if [[ !  `grep -ow "^GRUB_DEFAULT=\"$1>$2\"" /etc/default/grub` ]]; then

		sudo sed -i "s/^GRUB_DEFAULT=.*/GRUB_DEFAULT=\"$1>$2\"/" /etc/default/grub
		sudo grub-mkconfig -o /boot/grub/grub.cfg
		((count+=1))
	fi

	if [ $count -eq 2 ]; then
	
		echo -e "\e[32;1m::\e[37m GRUB config OK!\e[0m"
	fi
}


# Set frequency ucp (CPU laptop) AMD.
set_freq_ucp (){

	local min_freq=$1
	local max_freq=$2
	
	local rc=("governor='ondemand'"
	"min_freq=\"${min_freq}GHz\""
	"max_freq=\"${max_freq}GHz\"")

	source configlines/insertlines.sh "${rc[@]}" /etc/default/cpupower

	sudo systemctl enable cpupower.service
}


# DPI screen sway set. Los parámetros son $1=pulgadas $2=resolución (1366x768).
set_dpi_wayland(){

	#local RESOLUTION=$(xdpyinfo | awk '/dimensions/{print $2}')
	local WITDH=$(echo $2 | cut -d 'x' -f1)
	local HEIGHT=$(echo $2 | cut -d 'x' -f2)
	local DPI=$(echo "sqrt($WITDH^2+$HEIGHT^2)/$1" | bc)

	local dpisway=("QT_WAYLAND_FORCE_DPI=$DPI")

	source configlines/insertlines.sh "${dpisway[@]}" /etc/environment


}

# DPI screen Xorg set. Los parámetros son $1=pulgadas $2=resolución (1366x768).
set_dpi_xorg (){

	#local RESOLUTION=$(xdpyinfo | awk '/dimensions/{print $2}')
	local WITDH=$(echo $2 | cut -d 'x' -f1)
	local HEIGHT=$(echo $2 | cut -d 'x' -f2)
	local DPI=$(echo "sqrt($WITDH^2+$HEIGHT^2)/$1" | bc)
	sudo cp files/screen/10-monitor.conf /etc/X11/xorg.conf.d/
	check_fail $?
	sudo sed -i "s/96/$DPI/g" /etc/X11/xorg.conf.d/10-monitor.conf
	check_fail $?
	local num 

	if [ $DPI -ge 100 ]; then

		num=100
        elif [ $DPI -le 75 ]; then

		num=75
        elif [[ $(($DPI-75)) -lt $((100-$DPI)) ]]; then

		num=75
        else
		num=100
        fi	

	if [[ `loginctl show-session 1 -p Type | grep "x11"`  ]]; then

		sudo pacman -S xorg-fonts-${num}dpi --needed --noconfirm
	else
		sudo pacman -R xorg-fonts-${num}dpi
	fi
}


# Install AUR packages.
aur_packages (){

	echo -e "\e[34;1m::\e[37m AUR packages \e[34;1m::\e[0m"
	local packages=("$@")

	if [[ $USER != "root" ]]; then

		for package in ${packages[@]}
		do
			if  ! pacman -Qm $package &> /dev/null; then

				sudo rm -fr $package/
				echo "Instalando paquete "$package""
				# AUR packages.
				git clone https://aur.archlinux.org/$package.git && cd $package && makepkg -sic --noconfirm && cd .. && sudo rm -r $package
				cd ~/Git/dotfiles
			else
				echo -e "\e[34;1m  -->\e[37m Package '\e[0m"$package"\e[37;1m' previamente instalado.\e[0m"
				sudo rm -fr $package/
			fi
		done
	else
		echo -e "\e[33;1mWarning:\e[37m No está permitido usar makepkg por el usuario '\e[32m$USER\e[37m', puede causar daños.\e[0m"
	fi
}



# Qtile WM install in Xorg.
qtile_wm (){

	mkdir -p ~/.config/qtile/
	cp -u /usr/share/doc/qtile/default_config.py ~/.config/qtile/config.py
	check_fail $?

	if [[ ! `egrep "^#(xclock|xterm|exec xterm|twm)" /etc/X11/xinit/xinitrc` ]]; then

		sudo sed -i "/xclock\|xterm\|twm/s/^/#/g" /etc/X11/xinit/xinitrc
		check_fail $?
	fi

	cp /etc/X11/xinit/xinitrc ~/.xinitrc
	check_fail $?

	if [[ ! `grep "exec qtile" ~/.xinitrc` ]]; then	

		echo -e "while true; do\n\texec qtile\ndone" >> ~/.xinitrc
	fi
	echo -e "\e[32;1m::\e[37m Qtile OK!\e[0m"
}


# Sway WM install.
sway_wm (){

	mkdir -p ~/.config/sway/
	cp -u /etc/sway/config ~/.config/sway/config
}

# QT Wayland config.
wayland_qt_gtk (){

	sudo pacman -S qt5ct gtk4 --needed --noconfirm

	local waylandqtgtk=("QT_QPA_PLATFORMTHEME=qt5ct"
	#"QT_QPA_PLATFORM=xcb"
	"QT_QPA_PLATFORM=wayland"
	"_JAVA_AWT_WM_NONREPARENTING=1"
	"XDG_SESSION_TYPE=wayland")

	source configlines/insertlines.sh "${waylandqtgtk[@]}" /etc/environment

}

# All configurations sway.
sway_all (){

	sway_apps
	check_fail $?

	sway_wm
	check_fail $?

	sway_config
	check_fail $?

	wayland_qt_gtk
	check_fail $?

}
# Keyboard configuration.
sway_keyboard (){

	local model="$1"

	#local keyboard=("XKB_DEFAULT_LAYOUT=latam,us"
	local keyboard=("XKB_DEFAULT_LAYOUT=latam"
	"XKB_DEFAULT_MODEL=$model"
	"XKB_DEFAULT_VARIANT=deadtilde")
	#"XKB_DEFAULT_VARIANT=deadtilde,intl")

	source configlines/insertlines.sh "${keyboard[@]}" /etc/environment

	echo -e "\e[32;1m::\e[37m  Keyboard OK!\e[0m"
}


# makepkg config.
makepkg_setting  (){

	#sudo sed -i "/COMPRESSGZ/ s/-n)/-n -p)/ ; /COMPRESSBZ2/ s/-f)/-f -p\$\(nproc\))/ ; /COMPRESSXZ\|COMPRESSZST/ s/-)/- --threads=\$\(nproc\))/ ; /COMPRESSLRZ/ s/-q)/-q --threads \$\(nproc\))/ ; /^MAKEFLAGS/ s/[0-9]\+/\$\(nproc\)/ ; /MAKEFLAGS/ s/#// ; /MAKEFLAGS/ s/[0-9]\+/\$\(nproc\)/"   /etc/makepkg.conf
	sudo sed -i "/MAKEFLAGS=/ s/#\| //g ; /MAKEFLAGS=/ s/[0-9]\+/\$\(nproc\)/ ; /^COMPRESSGZ/ s/-n)/-n -p)/ ; /^COMPRESSBZ2/ s/-f)/-f -p\$\(nproc\))/ ; /^COMPRESSXZ\|^COMPRESSZST/ s/-)/- --threads=\$\(nproc\))/ ; /^COMPRESSLRZ/ s/-q)/-q --threads \$\(nproc\))/"  /etc/makepkg.conf
	echo -e "\e[32;1m::\e[37m MAKEPKG OK!\e[0m"
}

set_driver_intel (){

	# Filtre de manera silenciosa (sin salida).
	if  ! grep -q "^MODULES=(.*i915" /etc/mkinitcpio.conf ; then

		sudo sed -i 's/MODULES=(/MODULES=(i915 /' /etc/mkinitcpio.conf
		sudo mkinitcpio -P
		echo "Driver de video Intel OK."
		echo -e "\e[32;1m::\e[37m Driver de video Intel OK!\e[0m"

	else
		echo -e "\e[32;1m::\e[37m Driver de video Intel previamente establecido.\e[0m"
	fi
}

reflector_pacman (){

	sudo mkdir -p /etc/pacman.d/hooks/
	sudo cp -u files/mirrors/mirrorupgrade.hook /etc/pacman.d/hooks/
	sudo cp -u files/mirrors/reflector.conf /etc/xdg/reflector/
	echo -e "\e[32;1m::\e[37m Mirrors update reflector OK!\e[0m"

}

## power managment systemd.
power_managment (){

	local power=("HandlePowerKey=suspend"
	"HandleSuspendKey=suspend"
	"HandleHibernateKey=suspend")

	# Usar administración de energía de Systemd.
	source configlines/insertlines.sh "${power[@]}" /etc/systemd/logind.conf
	check_fail $?

	echo -e "\e[32;1m::\e[37m Power managment OK!\e[0m"
}


# Variables de entorno del usuario root.
root_environment (){

	# /etc/environment variables de entorno.
        local environment_root=("EDITOR=vim")

	source configlines/insertlines.sh "${environment[@]}" /etc/environment
	check_fail $?

}



# Kernel paramters sysctl ssd.
sysctl_ssd (){

	# /etc/sysctl configuración del sistema.
	local sysctl=("vm.swappiness=1"
	"net.ipv4.tcp_fastopen=3")

	source configlines/insertlines.sh "${sysctl[@]}" /etc/sysctl.d/99-sysctl.conf
	check_fail $?


}


# Kernel paramters sysctl hdd.
sysctl_hdd (){

	# /etc/sysctl configuración del sistema.
	local sysctl=("vm.swappiness=5"
	"net.ipv4.tcp_fastopen=3")
	"vm.vfs_cache_pressure=50"

	source configlines/insertlines.sh "${sysctl[@]}" /etc/sysctl.d/99-sysctl.conf
	check_fail $?


}

# Configuración de mpv y VAAPI.
set_vaapi (){

	# /etc/mpv/mpv.conf configuración de mpv.
	local mpv=('vo=gpu'
	'hwdec=vaapi'
	'fs=yes'
	'msg-level=vo=fatal'
	'cache=yes'
	'cache-pause=no'
	'alang=lat,spa'
	'slang=lat,spa'
	'ytdl-format=bestvideo[height<=?720][fps<=?30][vcodec!=?vp9]+bestaudio/best'
	'#ytdl-format=bestvideo[height<=?1080][fps<=?60][vcodec!=?vp9]+bestaudio/best')

	source configlines/insertlines.sh "${mpv[@]}" /etc/mpv/mpv.conf 
	check_fail $?

	# Link vaapi.
	sudo ln -sf /usr/lib/vdpau/libvdpau_va_gl.so.1 /usr/lib/libvdpau_i965.so
	check_fail $?

	 # /etc/environment variables de entorno.
        local environmentvaapi=("LIBVA_DRIVERS_PATH=/usr/lib/dri/"
	"LIBVA_DRIVER_NAME=i965"
	"VAAPI_MPEG4_ENABLED=true"
	"VDPAU_DRIVER=va_gl")

	source configlines/insertlines.sh "${environmentvaapi[@]}" /etc/environment
	check_fail $?

        
}


# Configuración de chromium con vaapi.
chromium_vaapi (){

	local chromvaapi=("--ignore-gpu-blocklist"
	"--enable-gpu-rasterization"
	"--enable-zero-copy"
	"--enable-accelerated-video-decode")

	source configlines/insertlines.sh "${chromvaapi[@]}" ~/.config/chromium-flags.conf
}


# Alternar automáticamente la conexión inalámbrica en función del cable LAN.
automatically_toggle_wifi_lan (){

	sudo cp -u files/networkmanager/wlan_auto_toggle.sh /etc/NetworkManager/dispatcher.d/
	sudo chmod u+x /etc/NetworkManager/dispatcher.d/wlan_auto_toggle.sh
}


# User setting tmux.
tmux_setting (){

	local tmux=("unbind C-b"
	"set -g prefix C-a"
	"bind C-a send-prefix")
	
	source configlines/insertlines.sh "${tmux[@]}" ~/.tmux.conf

}

# Configuración del prompt bash.
set_prompt (){

	sudo cp /usr/share/git/completion/git-completion.bash ~/.git-completion.bash
	sudo cp /usr/share/git/completion/git-prompt.sh ~/.git-prompt

	local gitbash=(". ~/git-completion.bash"
	". ~/git-prompt.sh"
	"export GIT_PS1_SHOWDIRTYSTATE=1")
}


## Configurations for the xorg server.

# Setting up the Xterm terminal in Xorg.
setting_xterm (){

	local xresources=("XTerm.vt100.foreground:MistyRose3"
	"XTerm.vt100.background:gray6"
	"XTerm.vt100.faceName:Dejavu Sans Mono:size=10"
	"XTerm.termName:xterm-256color"
	"XTerm.vt100.selectToClipboard:true")

	source configlines/insertlines.sh "${xresources[@]}" ~/.Xresources 
	check_fail $?
}

# Fix Xorg error cannot find acpid.service.
fix_acpid (){

	# Evitar que Xorg exija acpid.service.
	sudo cp files/Xorg/xorg.conf /etc/X11/xorg.conf
}


# Settins Git.
git_user_config (){

	# Si el usuario dakataca no está configurado con git.
	if [[ ! `git config --list | grep "user.name=dakataca"` ]]; then

		git config --global user.name dakataca
		git config --global user.email danieldakataca@gmail.com
		git config --global core.editor vim
		git config --global diff.tool vimdiff
		git config --global core.autocrlf input

		echo -e "\e[32;1m::\e[37m User dakata on Git OK!\e[0m"
	else
		echo -e "\e[32;1m::\e[37m User dakata on Git OK!\e[0m"
	fi
}



# Settings ~/.bashrc.
bashrc_settings (){

   	local bashrc=("alias grep='grep --color'"
	"alias egrep='egrep --color'"
        "export GREP_COLOR='01;35'")

	source configlines/insertlines.sh "${bashrc[@]}" ~/.bashrc
	check_fail $?
}


# AUR packages for xorg.
aur_xorg_apps (){

	aur_packages "paru upd72020x-fw peerflix"
}

# AUR packages for xorg.
aur_gnome_apps (){

	aur_packages "paru upd72020x-fw peerflix kooha obs-v4l2sink obs-gnome-screencast"
}

## Ejecución general para todos los equipos.
general_settings (){
get_procesor_features
	echo -e "\e[34;1m::\e[37m General settings \e[34;1m::\e[0m"

	makepkg_setting
	check_fail $?

	reflector_pacman
	check_fail $?

	power_managment
	check_fail $?

	automatically_toggle_wifi_lan
	check_fail $?
	
	sysctl_ssd
	check_fail $?

	root_environment
	check_fail $?

	# Configuraciones para usuarios normales distintos de root.
	if [[ $USER != "root" ]]; then


		git_user_config
		check_fail $?

		bashrc_settings		
		check_fail $?

		tmux_setting
		check_fail $?
	fi
}


# inhabilitar temporalmenre el funcionamiento del screensaver en gnome.
gnome_screensaver_inhibit (){

	local screensaverinhibit=("alias qbittorent='gnome-session-inhibit --inhibit suspend qbittorrent'"
	"alias uget-gtk='gnome-session-inhibit --inhibit suspend uget-gtk'"
	"alias mpv='gnome-session-inhibit --inhibit idle mpv'")

	source configlines/insertlines.sh "${screensaverinhibit[@]}" /etc/bash.bashrc 
	source /etc/bash.bashrc
}

gnome_wayland (){

	sudo pacman -Syu ${DEGNOME[@]} ${INTEL_UTILS[@]} ${ARCHIVING[@]} ${VAAPI[@]} ${SOUND_BLUETOOTH[@]} ${MULTIMEDIA[@]} ${SCREEN_CAPTURE_WAYLAND[@]} ${EDITION[@]} ${MESSAGING[@]} ${INTERNET[@]} ${ANDROID[@]} ${SYSTEM_TOOLS[@]} ${USER_TOOLS[@]} --needed --noconfirm

	# Habilito el administrador de sesiones gdm.
	sudo systemctl enable gdm.service

	# Configuraciones para usuarios normales distintos de root.
	if [[ $USER != "root" ]]; then

		aur_gnome_apps	
	else
		echo "Para poder instalar aplicaciones de AUR debe iniciar con un usuario distinto de root."
	fi

	gnome_screensaver_inhibit
	check_fail $?
}


## Main

#if [[ $(sudo dmesg | grep "DMI\|Hardware name" | head -n1 | cut -d ':' -f2) =~ "Hewlett-Packard HP Mini 210-1000/3660" ]]; then
if [[ $(sudo dmesg | grep "Hewlett-Packard HP Mini 210-1000/3660") ]]; then

	general_settings

        # /etc/environment variables de entorno.
        environment=("LIBVA_DRIVER_NAME=i965"
        "EDITOR=vim"
        "MESA_LOADER_DRIVER_OVERRIDE=i965"
        "LIBGL_DRI3_DISABLE=1")

	source configlines/insertlines.sh "${environment[@]}" /etc/environment
	check_fail $?

	# /etc/sysctl configuración del sistema.
	sysctl=("vm.swappiness=0"
	"vm.vfs_cache_pressure=50"
	"net.ipv4.tcp_fastopen=3")

	source configlines/insertlines.sh "${sysctl[@]}" /etc/sysctl.d/99-sysctl.conf
	check_fail $?

	# Ficheros de configuración estructurados.


	# Genero ficheros de configuración.
	cp hpmini/files/alsa/asoundrc ~/.asoundrc
	check_fail $?
        sudo cp "hpmini/files/Driver de video intel/20-intel.conf" /etc/X11/xorg.conf.d/20-intel.conf
	check_fail $?
	echo -e "\e[32;1m::\e[37m Dotfiles del usuario \e[32m$USER\e[0m\e[37;1m esablecidos correctamente.\e[0m"	

#elif [[ $(sudo dmesg | grep "DMI\|Hardware name" | head -n1 | cut -d ':' -f2) =~ "Hewlett-Packard Compaq Presario CQ40 Notebook PC/3607" ]]; then
elif [[ $(sudo dmesg | grep "Hewlett-Packard Compaq Presario CQ40 Notebook PC/3607") ]]; then

	
	# Aplicaciones presario CQ40.
	source presarioCQ40/apps.sh
	check_fail $?

	general_settings

	set_dpi_xorg	14 "1280x800" 

        # /etc/environment variables de entorno.
        environment=("LIBVA_DRIVERS_PATH=/usr/lib/dri/"
	"LIBVA_DRIVER_NAME=i965"
        "MESA_LOADER_DRIVER_OVERRIDE=i965"
	"VDPAU_DRIVER=va_gl")

	source configlines/insertlines.sh "${environment[@]}" /etc/environment
	check_fail $?

	# /etc/sysctl configuración del sistema.
	sysctl=("vm.swappiness=5"
	"vm.vfs_cache_pressure=50"
	"net.ipv4.tcp_fastopen=3")

	source configlines/insertlines.sh "${sysctl[@]}" /etc/sysctl.d/99-sysctl.conf
	check_fail $?

	# /etc/mpv/mpv.conf configuración de mpv.
	mpv=('vo=gpu'
	'hwdec=vdpau'
	'fs=yes'
	'msg-level=vo=fatal'
	'cache=yes'
	'cache-pause=no'
	'alang=lat,spa'
	'slang=lat,spa'
	'ytdl-format=bestvideo[height<=?720][fps<=?30][vcodec!=?vp9]+bestaudio/best'
	'#ytdl-format=bestvideo[height<=?1080][fps<=?60][vcodec!=?vp9]+bestaudio/best')

	source configlines/insertlines.sh "${mpv[@]}" /etc/mpv/mpv.conf 
	check_fail $?

	## Ficheros de configuración estructurados.

	# Driver de video Intel pentium.
	sudo cp "presarioCQ40/files/Driver de video intel modesetting/20-gpudriver.conf" /etc/X11/xorg.conf.d/20-gpudriver.conf
	check_fail $?

	set_driver_intel

#elif [[ $(sudo dmesg | grep "DMI\|Hardware name" | head -n1 | cut -d ':' -f2) =~ "Acer Aspire ES1-411/EA42_BM" ]]; then
elif [[ $(sudo dmesg | grep "Acer Aspire ES1-411/EA42_BM") ]]; then

	gnome_wayland
	check_fail $?

	set_dpi_wayland 14 "1366x768"
	check_fail $?

	# Configuraciones independientes del servidor gráfico.

	general_settings
	check_fail $?

	set_driver_intel
	check_fail $?

	set_vaapi
	check_fail $?

	chromium_vaapi
	check_fail $?

	## Ficheros de configuración estructurados.

#elif [[ $(sudo dmesg | grep "DMI\|Hardware name" | head -n1 | cut -d ':' -f2) =~ "ASUSTeK COMPUTER INC. X555LB/X555LB" ]]; then
elif [[ $(sudo dmesg | grep "ASUSTeK COMPUTER INC. X555LB/X555LB") ]]; then

	# Aplicaciones del Asus K555LB
	source K555LB/apps.sh
	check_fail $?

	# ~/.config/chromium-flags.conf configuración de chromium
	chromium=('--disable-gpu')

	general_settings

	set_dpi_xorg	15.6 "1366x768"

	sound_intel=("options snd-hda-intel index=-1"
	"options snd-hda-intel index=-2")

	source configlines/insertlines.sh "${sound_intel[@]}" /etc/modprobe.d/alsa-base.conf 
	check_fail $?
	
        # /etc/environment variables de entorno.
        environment=("LIBVA_DRIVERS_PATH=/usr/lib/dri/"
	"LIBVA_DRIVER_NAME=i965"
	"VAAPI_MPEG4_ENABLED=true"
	"EDITOR=vim")

	source configlines/insertlines.sh "${environment[@]}" /etc/environment
	check_fail $?

	# /etc/sysctl configuración del sistema.
	sysctl=("vm.swappiness=15"
	"vm.vfs_cache_pressure=50"
	"net.ipv4.tcp_fastopen=3")

	source configlines/insertlines.sh "${sysctl[@]}" /etc/sysctl.d/99-sysctl.conf
	check_fail $?

	set_vaapi

	## Ficheros de configuración estructurados.
	
	# Driver de video Intel I7 5500.
	sudo cp "K555LB/files/Driver de video intel modesetting/20-gpudriver.conf" /etc/X11/xorg.conf.d/20-gpudriver.conf
	check_fail $?

	set_driver_intel

#elif [[ $(sudo dmesg | grep "DMI\|Hardware name" | head -n1 | cut -d ':' -f2) =~ "Hewlett-Packard Presario CQ42 Notebook PC/1444" ]]; then
elif [[ $(sudo dmesg | grep "Hewlett-Packard Presario CQ42 Notebook PC/1444") ]]; then

	grub_config 2 1

	# Aplicaciones del presario CQ42. 
	source presarioCQ42/apps.sh
	check_fail $?

	general_settings

	set_dpi_xorg	14 "1280x800" 

	# /etc/environment variables de entorno.
        environment=("LIBVA_DRIVERS_PATH=/usr/lib/dri/"
	"VDPAU_DRIVER=va_gl"
	"EDITOR=vim")

	source configlines/insertlines.sh "${environment[@]}" /etc/environment
	check_fail $?

	# /etc/sysctl configuración del sistema.
	sysctl=("vm.swappiness=5"
	"vm.vfs_cache_pressure=50"
	"net.ipv4.tcp_fastopen=3")

	source configlines/insertlines.sh "${sysctl[@]}" /etc/sysctl.d/99-sysctl.conf
	check_fail $?

	# /etc/mpv/mpv.conf configuración de mpv.
	mpv=('vo=gpu'
	'hwdec=vdpau'
	'fs=yes'
	'msg-level=vo=fatal'
	'cache=yes'
	'cache-pause=no'
	'alang=lat,spa'
	'slang=lat,spa'
	'ytdl-format=bestvideo[height<=?720][fps<=?30][vcodec!=?vp9]+bestaudio/best'
	'#ytdl-format=bestvideo[height<=?1080][fps<=?60][vcodec!=?vp9]+bestaudio/best')

	source configlines/insertlines.sh "${mpv[@]}" /etc/mpv/mpv.conf 
	check_fail $?

	set_freq_ucp "0.8" "2"

	#driver video radeon HD 4200.
	sudo cp presarioCQ42/files/Driver\ video\ radeon\ HD\ 4200/20-radeon.conf /etc/X11/xorg.conf.d/

#elif [[ $(sudo dmesg | grep "DMI\|Hardware name" | head -n1 | cut -d ':' -f2) =~ "ASUSTeK COMPUTER INC. X540LA/X540LA" ]]; then
elif [[ $(sudo dmesg | grep "ASUSTeK COMPUTER INC. X540LA/X540LA") ]]; then

	# Aplicaciones del Asus X540LA 
	source X540LA/apps.sh
	check_fail $?

	set_vaapi

	# /etc/environment variables de entorno.
        environment=("LIBVA_DRIVERS_PATH=/usr/lib/dri/"
	"LIBVA_DRIVER_NAME=iHD"
	"VAAPI_MPEG4_ENABLED=true")

	source configlines/insertlines.sh "${environment[@]}" /etc/environment
	check_fail $?
else
	echo -e "\e[33;1mWarning:\e[37m Equipo desconocido.\e[0m"
fi
